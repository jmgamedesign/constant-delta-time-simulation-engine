package testbed;

import simulation.Simulation;
import simulation.SimulationConfig;
import simulation.Updatable;

import java.lang.reflect.Array;

public class CDTSETestbed
{
    public static void main (String[] arg) throws Exception
    {
        Simulation sim = new Simulation(new SimulationConfig(50));
        sim.primeSimulation();

        //This runs 10_000 * (100 * 3) square roots each tick, or 3_000_000 square roots each tick.
        for(int i = 0; i < 10_000; i++)
        {
            sim.queueAddingUpdatable(new Updatable()
            {
                @Override
                protected void updateInit() throws Exception
                {
                    for(int i = 0; i < 100; i++)
                    {
                        Math.sqrt(999_999);
                    }
                }

                @Override
                protected void update() throws Exception
                {
                    for(int i = 0; i < 100; i++)
                    {
                        Math.sqrt(999_999);
                    }
                }

                @Override
                protected void updateFinish() throws Exception
                {
                    for(int i = 0; i < 100; i++)
                    {
                        Math.sqrt(999_999);
                    }
                }
            });
        }

        sim.startSimulation();

        double efficiency = 100.0;
        int i = 0;

        while (true)
        {
            if(i == 100_000_000)
            {
                i = 0;
                efficiency = -1;
            }

            double currentEfficiency = sim.getSimulationEfficiency();

            if(efficiency != currentEfficiency)
            {
                efficiency = currentEfficiency;
                System.out.println(efficiency);
            }

            i++;
        }
    }
}
