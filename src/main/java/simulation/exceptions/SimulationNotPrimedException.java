package simulation.exceptions;

public class SimulationNotPrimedException extends RuntimeException
{
    public SimulationNotPrimedException()
    {
        super("Call primeSimulation() before interacting with the Simulation object.");
    }
}
