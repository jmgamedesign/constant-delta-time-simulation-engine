package simulation.exceptions;

public class IllegalUpdatableCallException extends Exception
{
    public IllegalUpdatableCallException()
    {
        super("Do not manually call an Updatable object's call() method.");
    }
}
