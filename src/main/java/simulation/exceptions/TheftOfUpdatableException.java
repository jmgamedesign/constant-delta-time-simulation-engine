package simulation.exceptions;

public class TheftOfUpdatableException extends RuntimeException
{
    public TheftOfUpdatableException()
    {
        super("Updatables can only ever belong to a single Simulation object.");
    }
}
