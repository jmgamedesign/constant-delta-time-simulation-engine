package simulation;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinWorkerThread;


final class SimulationWorkerThreadFactory implements ForkJoinPool.ForkJoinWorkerThreadFactory
{
    private final Simulation owner;
    private final int priority;

    SimulationWorkerThreadFactory(Simulation _owner, int _threadPriority)
    {
        owner = _owner;

        priority = _threadPriority;
    }

    public final SimulationWorkerThread newThread(ForkJoinPool _pool)
    {
        SimulationWorkerThread thread = new SimulationWorkerThread(owner, _pool);
        thread.setPriority(priority);

        return thread;
    }

    static final class SimulationWorkerThread extends ForkJoinWorkerThread
    {
        final Simulation owner;

        SimulationWorkerThread(Simulation _owner, ForkJoinPool _pool)
        {
            super(_pool);

            owner = _owner;
        }
    }
}
