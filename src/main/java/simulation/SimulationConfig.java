package simulation;

public class SimulationConfig
{
    private static final double ONE_SECOND = 1.0;
    private static final double MS_PER_SECOND = 1000.0;

    public final int simulationTicksPerSimulationSecond;
    public final int targetTimeInMSPerUberTick;

    public final double deltaTime;
    public final double deltaTime2;

    public final double maxSpeed;
    public final double maxSpeedTimesDeltaTime;

    public SimulationConfig(double _maxSpeed)
    {
        /*
        These values for _simulationTicksPerSimulationSecond and _targetTimeInMSPerUberTick
        were specially chosen to allow the speed of the simulation to change
        in increments of 50%, and to ensure any draw threads have a good likelihood
        to get around 60 simulation updates per second. The reasons for why
        it works like this are strange and complex, and involve how
        the SimulationThread objects try to maintain a constant rate of ticks per second.

        The short of it is in the worst case fast computers will see uberticks
        as single ticks with unusually good precision.
        */
        this(125,
                16,
                _maxSpeed);
    }

    public SimulationConfig(int _simulationTicksPerSimulationSecond,
                            int _targetTimeInMSPerUberTick,
                            double _maxSpeed)
    {
        simulationTicksPerSimulationSecond = _simulationTicksPerSimulationSecond;
        targetTimeInMSPerUberTick = _targetTimeInMSPerUberTick;

        deltaTime = ONE_SECOND / simulationTicksPerSimulationSecond;
        deltaTime2 = deltaTime * deltaTime;

        maxSpeed = _maxSpeed;
        maxSpeedTimesDeltaTime = maxSpeed * deltaTime;
    }

    final int getUberTick(double _simulationSpeed)
    {
        double simulationTicksPerIRLSecond = Math.round(simulationTicksPerSimulationSecond * _simulationSpeed);

        /*
        The number of ticks in an ubertick must be an integer value.
        Constant Delta Time Simulation Engine does not have support for partial ticks.
        Should the value be 0 or negative the simulation thread will
        simply pass through the tick loop, which will effectively pause the simulation.
        It's up to the programmer to not put absurd numbers into the SimulationConfig
        or simulationSpeed variables.
        */
        return (int) Math.round(simulationTicksPerIRLSecond * targetTimeInMSPerUberTick / MS_PER_SECOND);
    }
}
