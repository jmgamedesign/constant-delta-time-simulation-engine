package simulation;

enum UpdatePhase
{
    NONE,
    UPDATEINIT,
    UPDATE,
    UPDATEFINISH
}
