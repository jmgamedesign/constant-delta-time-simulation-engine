package simulation;

import concurrent.delayed.delayed_collection.IDelayedCollection;
import concurrent.delayed.delayed_collection.single_owner.DelayedSingleOwnerHashSet;
import simulation.exceptions.SimulationNotPrimedException;

import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

public final class Simulation
{
    public final SimulationConfig config;

    public final int threadPriority;

    private ExecutorService executor;

    public Simulation(SimulationConfig _config)
    {
        this(_config, Thread.NORM_PRIORITY);
    }

    public Simulation(SimulationConfig _config, int _threadPriority)
    {
        if (_config == null)
        {
            throw new NullPointerException("Null _config object passed into Simulation's constructor method.");
        }

        config = _config;

        if (_threadPriority < Thread.MIN_PRIORITY)
        {
            _threadPriority = Thread.MIN_PRIORITY;
        }

        if (_threadPriority > Thread.MAX_PRIORITY)
        {
            _threadPriority = Thread.MAX_PRIORITY;
        }

        threadPriority = _threadPriority;
    }

    public synchronized void primeSimulation()
    {
        if (executor == null)
        {
            /*
            This is a modified newWorkStealingPool that uses a custom
            thread factory to insert information about this Simulation object
            into the threads. This is important to prevent illegal access to
            the call() methods of Updatable objects. Technically this is
            a violation of the Callable interface, but programmers are not
            intended to interact with this part of the Constant Delta Time Simulation Engine API.
            This is an example of extremely defensive programming to prevent race conditions.

            To see how this is a modified newWorkStealingPool, see Executors.newWorkStealingPool();

            The reason why Simulation needs to be primed before use is because
            the "this" keyword in Java cannot be used in constructors.
             */
            executor = new ForkJoinPool(Runtime.getRuntime().availableProcessors(),
                    new SimulationWorkerThreadFactory(this, threadPriority),
                    null,
                    true);
        }
    }

    //#region simulation

    //This is the thread that will do work on the simulation as it advances.
    private volatile SimulationThread simulationThread;

    //#region simulation controls

    //This variable affects how long it takes for a simulation second to pass in real time.
    private volatile double simulationSpeed = 1.0;

    public final synchronized void startSimulation()
    {
        startSimulation(simulationSpeed);
    }

    public volatile int simulationThreadCount = 0;

    public final synchronized void startSimulation(double _simulationSpeed)
    {
        if (executor == null)
        {
            throw new SimulationNotPrimedException();
        }

        if (simulationThread == null)
        {
            simulationThread = new SimulationThread(this);
            setSimulationSpeed(_simulationSpeed);
            simulationThread.setDaemon(true);
            simulationThread.setPriority(threadPriority);
            simulationThread.setName("Simulation Thread #" + simulationThreadCount);
            int simCount = simulationThreadCount + 1;
            simulationThreadCount = simCount;
            simulationThread.start();
        }
    }

    public final synchronized void stopSimulation()
    {
        if (executor == null)
        {
            throw new SimulationNotPrimedException();
        }

        //This triggers a later check that will cause SimulationThread threads to halt.
        simulationThread = null;
    }

    public final synchronized boolean isSimulationRunning()
    {
        if (executor == null)
        {
            throw new SimulationNotPrimedException();
        }

        /*
        Setting simulationThread to null necessarily kills
        the SimulationThread thread, so this check is sufficient
        to determine if the simulation is running.
         */
        return simulationThread != null;
    }

    public final synchronized void setSimulationSpeed(double _simulationSpeed)
    {
        if (executor == null)
        {
            throw new SimulationNotPrimedException();
        }

        simulationSpeed = _simulationSpeed;

        if (simulationThread != null)
        {
            simulationThread.setSimulationSpeed(_simulationSpeed);
        }
    }

    public final synchronized double getSimulationSpeed()
    {
        if (executor == null)
        {
            throw new SimulationNotPrimedException();
        }

        return simulationSpeed;
    }

    public final synchronized boolean isSimulationPaused()
    {
        if (executor == null)
        {
            throw new SimulationNotPrimedException();
        }

        /*
        Note the difference between a running simulation and a paused simulation:

        A paused simulation might still be running, but the SimulationThread thread
        will not advance the simulation. A simulation that is not running cannot
        advance the simulation even if it's not paused because there is no
        SimulationThread thread that can do work on it.

        Also note this check does not look to see if the simulation speed is 0.
        This is because, depending on the settings in the SimulationConfig object,
        a low enough simulation speed can starve the SimulationThread thread,
        so this check specifically has to check for starvation.
         */
        return config.getUberTick(simulationSpeed) == 0;
    }

    //#endregion

    //#region simulation efficiency

    //This is a measurement of how well the CPU is able to maintain a constant rate of simulation ticks.
    private volatile double simulationEfficiency = 100;

    final void postSimulationEfficiency(double _simulationEfficiency)
    {
        /*
        Race conditions on this variable do not matter simple because
        this variable is for diagnostics and logging purposes only.
        No important game logic should depend on this value ever.
         */
        simulationEfficiency = _simulationEfficiency;
    }

    public final double getSimulationEfficiency()
    {
        if (executor == null)
        {
            throw new SimulationNotPrimedException();
        }

        return simulationEfficiency;
    }

    //#endregion

    /*
    This is the IDelayedCollection of all Updatable objects that exist within the simulation.
    Updatable objects can be particles, AI-driven agents, physics bodies,
    and even abstract controller objects.

    Note a DelayedSingleOwnerHashSet object is being used here because
    only one thread at a time is expected to be able to work with this object.
    Ownership gets passed off, not shared.
     */
    private final IDelayedCollection<Updatable> updatablesDelayedCollection = new DelayedSingleOwnerHashSet<>();

    //#region adding and removing queueing

    /*
    queueAddingUpdatable() and queueRemovingUpdatable() do not directly
    add or remove Updatable objects to/from this Simulation object.
    This is to prevent nasty race conditions that can plague Collections.
    Instead these methods use an IDelayedCollection object to asynchronously
    communitcate with the simulation.
     */

    public final void queueAddingUpdatable(Updatable _updatable)
    {
        if (executor == null)
        {
            throw new SimulationNotPrimedException();
        }

        if (_updatable == null)
        {
            throw new NullPointerException("Null Updatable object passed into Simulation's queueAddUpdatable() method.");
        }

        /*
        To prevent data ownership issues this Simulation object will
        try to make an immutable claim on any Updatable objects added to it.
        If the claim fails the TheftOfUpdatableException will be thrown
        by Updatable.setOwner().
         */
        _updatable.setOwner(this);

        updatablesDelayedCollection.queueAddingElement(_updatable);
    }

    public final void queueRemovingUpdatable(Updatable _updatable)
    {
        if (executor == null)
        {
            throw new SimulationNotPrimedException();
        }

        if (_updatable == null)
        {
            throw new NullPointerException("Null Updatable object passed into Simulation's queueRemoveUpdatable() method.");
        }

        updatablesDelayedCollection.queueRemovingElement(_updatable);
    }

    //#endregion

    /*
    This keeps track of where this Simulation object is in its tick() method.
    Updatable objects read this through the getUpdatePhase() method to understand
    which update phase they need to execute when ExecutorService.invokeAll()
    causes their call() methods to run.
     */
    private volatile UpdatePhase updatePhase = UpdatePhase.NONE;

    final UpdatePhase getUpdatePhase()
    {
        return updatePhase;
    }

    final synchronized boolean tick()
    {
        /*
        This is the primary means by which stopSimulation() works.
        It also ensures that no matter what only this Simulation's
        SimulationThread thread can call tick().

        Because tick() is synchronized there's no need to worry
        about a race condition if someone does this, for example:

            startSimulation();
            stopSimulation();
            startSimulation();

        The stopping SimulationThread thread will have to finish its work
        before the new SimulationThread thread will be able to start working.
        */
        if (Thread.currentThread() != simulationThread)
        {
            //When this returns false a SimulationThread thread will immediately terminate.
            return false;
        }

        try
        {
            Collection<Updatable> updatablesCollection = updatablesDelayedCollection.getCollectionUnsafe();

            updatePhase = UpdatePhase.UPDATEINIT;
            executor.invokeAll(updatablesCollection);

            updatePhase = UpdatePhase.UPDATE;
            executor.invokeAll(updatablesCollection);

            updatePhase = UpdatePhase.UPDATEFINISH;
            executor.invokeAll(updatablesCollection);

            updatePhase = UpdatePhase.NONE;
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        /*
        This is how the queueAddingUpdatable() and queueRemovingUpdatable() methods
        communicate with the ongoing simulation. They aggregate writes that need
        to be made to the simulation, and then at the end of each tick the writes are finished.
         */
        updatablesDelayedCollection.executeWrites();

        return true;
    }

    //#endregion
}
