package simulation;

final class SimulationThread extends Thread
{
    private final Simulation simulation;
    private final SimulationConfig config;

    SimulationThread(Simulation _simulation)
    {
        simulation = _simulation;
        config = simulation.config;
    }

    //#region Simulation Speed

    //This variable affects how long it takes for a simulation second to pass in real time.
    private volatile double simulationSpeed = 1.0;

    final void setSimulationSpeed(double _simulationSpeed)
    {
        simulationSpeed = _simulationSpeed;
    }

    //#endregion

    @Override
    public final void run()
    {
        while (true)
        {
            long startTime = System.currentTimeMillis();

            int uberTick = config.getUberTick(simulationSpeed);

            for (int i = 0; i < uberTick; i++)
            {
                /*
                This returns false if this SimulationThread thread
                does not have permission to advance the simulation.
                 */
                if (!simulation.tick())
                {
                    return;
                }
            }

            long endTime = (System.currentTimeMillis() - startTime);

            long sleepTime = config.targetTimeInMSPerUberTick - endTime;


            double simulationEfficiency = 100;

            if (sleepTime > 0)
            {
                try
                {
                    Thread.sleep(sleepTime);
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            } else
            {
                double totalPercentTime = 1.0 - (double) sleepTime / config.targetTimeInMSPerUberTick;

                simulationEfficiency /= totalPercentTime;
            }

            simulation.postSimulationEfficiency(simulationEfficiency);
        }
    }
}
