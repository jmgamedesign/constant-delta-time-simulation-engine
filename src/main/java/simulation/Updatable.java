package simulation;

import simulation.exceptions.IllegalUpdatableCallException;
import simulation.exceptions.TheftOfUpdatableException;

import java.util.concurrent.Callable;

public abstract class Updatable implements Callable<Object>
{
    private volatile Simulation owner = null;

    final synchronized void setOwner(Simulation _owner)
    {
        if (owner != null && owner != _owner)
        {
            throw new TheftOfUpdatableException();
        }

        owner = _owner;
    }

    @Override
    public synchronized final Object call() throws Exception
    {
        /*
        If an unauthorized thread tries to call call(),
        or no Simulation object has claimed this Updatable object,
        then call() will fail, thus allowing this method to safely
        be a public method, which is necessary for compatibility
        with the ExecutorService API.
         */
        SimulationWorkerThreadFactory.SimulationWorkerThread workerThread;
        try
        {
            workerThread = (SimulationWorkerThreadFactory.SimulationWorkerThread) Thread.currentThread();
        } catch (ClassCastException e)
        {
            throw new IllegalUpdatableCallException();
        }

        if (workerThread.owner != owner || owner == null)
        {
            throw new IllegalUpdatableCallException();
        }

        /*
        To prevent Simulation.tick() from needing to perform expensive,
        single-threaded iterations over its Set of Updatable objects
        ExecutorService.invokeAll() is simply given the Set of
        Updatable objects, and they switch to the appropriate
        update phase.
         */
        UpdatePhase phase = owner.getUpdatePhase();
        switch (phase)
        {
            case UPDATEINIT:
                updateInit();
                break;

            case UPDATE:
                update();
                break;

            case UPDATEFINISH:
                updateFinish();
                break;

            default:
                break;
        }

        /*
        The ExecutorService API wants Callable objects to return something
        because the ExecutorService.invokeAll() method will return a List of
        Future objects, which include meta data about the execution of their
        associated Callable objects. Whatever is returned here goes into a
        Future object. The "problem" is Constant Delta Time Simulation Engine
        doesn't care about the Future objects, which is why the return type here is
        Object, and call() always returns null.

        Constant Delta Time Simulation Engine is using ExecutorService.invokeAll()
        to take advantage of work-stealing thread pools to ensure all
        Updatable objects are updated as efficiently as possible.
        ExecutorService.invokeAll() is also being used because it blocks until
        all tasks are completed, which is necessary to prevent race conditions
        between the three update phases.

        Perhaps there is a use for the Lists of Future objects, but for
        speed reasons Constant Delta Time Simulation Engine is choosing
        to ignore them.
        */
        return null;
    }

    //#region update phases

    protected void updateInit() throws Exception
    {
    }

    protected void update() throws Exception
    {
    }

    protected void updateFinish() throws Exception
    {
    }

    //#endregion
}
